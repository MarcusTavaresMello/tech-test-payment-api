using Microsoft.AspNetCore.Mvc;
using APIPay.Models;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]


public class SaleController : ControllerBase
{
    private readonly List<Venda> vendas = new();

    [HttpGet("{id}")]
    public IActionResult GetToId(int id)
    {
        return Ok(vendas[id]);
    }

    [HttpPost]
    public IActionResult NewSale(Venda venda)
    {
        if (venda.ProdutosVendidos.Count < 1)
            return BadRequest("Carrinho vazio. Nenhum item adicionado.");

        vendas.Add(venda);

        return Ok("Venda incluída com sucesso.");
        
    }


    [HttpPut("{id}")]
    public IActionResult UpdateStatus(int id, Venda venda)
    {
        if(vendas[id].Status.ToString() == "Aguardando_Pgto")
        {
            if (venda.Status.ToString() == "Pgto_Aprovado" || venda.Status.ToString() == "Venda_Cancelada")
                vendas[id] = venda;
        }
        else if(vendas[id].Status.ToString() == "Pgto_Aprovado")
        {
            if (venda.Status.ToString() == "Enviado_Transportadora" || venda.Status.ToString() == "Venda_Cancelada")
                vendas[id] = venda;
        }
        else if(vendas[id].Status.ToString() == "Enviado_Transportadora")
        {
            if (venda.Status.ToString() == "produto_entregue")
                vendas[id] = venda;
        }


        return Ok("Status Atualizada.");
    }


    [HttpDelete("{id}")]
    public IActionResult DeleteSale(Venda venda)
    {
        vendas.Remove(venda);

        return Ok("Venda excluída com sucesso.");
    }
}