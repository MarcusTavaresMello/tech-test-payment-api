using Microsoft.AspNetCore.Mvc;
using APIPay.Models;


namespace tech_test_payment_api.Controllers;


[ApiController]
[Route("[controller]")]
public class SellerController : ControllerBase
{
    private  List<Vendedor> vendedores = new();



    [HttpGet("{id}")]
    public IActionResult GetToId(int id)
    {
        return Ok(vendedores[id]);
    }



    [HttpPost]
    public IActionResult NewSeller(Vendedor vendedor)
    {
        vendedores.Add(vendedor);

        return Ok("Novo vendedor cadastrado com sucesso.");
        
    }



    [HttpPut("{id}")]
    public IActionResult UpdateStatus(int id, Vendedor vendedor)
    {
        vendedores[id] = vendedor;

        return Ok("Status atualizado.");
    }



    [HttpDelete("{id}")]
    public IActionResult DeleteSeller(Vendedor vendedor)
    {
        vendedores.Remove(vendedor);

        return Ok("Cadastro de vendedor excluído com sucesso.");
    }
}