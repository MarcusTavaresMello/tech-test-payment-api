using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PayAPI.Models;
using PayAPI.Views;
using PayAPI.Repositories;


namespace PayAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly SaleRepository _repository;
        public SaleController(SaleRepository repository)
        {
            _repository = repository;
        }
        private static string txtNoFound(Guid idVenda)
        {
            return $"A ID : {idVenda} não está vinculada a nenhuma venda.";
        }



        [HttpPost]
        public async Task<ActionResult<SALE>> newSALE([FromBody] SALE venda)
        {
            if (ModelState.IsValid)
            {
                var result = await _repository.newSALE(venda);
                return CreatedAtAction("newSALE", result);
            }
            return BadRequest(ModelState);
        }



        [HttpGet("{idVenda}")]
        public async Task<IActionResult> GetSaleToId(Guid idVenda)
        {
            var vendaRegistrada = await _repository.getSale(idVenda);
            if (vendaRegistrada == null) return NotFound(txtNoFound(idVenda));
            SaleView venda = new SaleView(
                vendaRegistrada.Id,
                vendaRegistrada.Data,
                vendaRegistrada.Status,
                vendaRegistrada.Vendedor,
                vendaRegistrada.Itens);
            return Ok(venda);
        }



        [HttpPut("{idVenda}")]
        public async Task<IActionResult> updateStatus(Guid idVenda, [FromBody] StatusSALE newStatus)
        {
            SALE vendaRegistrada = await _repository.getSale(idVenda);
            if (vendaRegistrada == null) return NotFound(txtNoFound(idVenda));
            var errorStatus = vendaRegistrada.modifyStatus(newStatus.Status);
            if (errorStatus == "")
            {
                _repository.modifyStatus(vendaRegistrada);
                return NoContent();
            }
            return BadRequest(errorStatus);
        }
    }
}
