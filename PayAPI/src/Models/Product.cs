using System;

namespace PayAPI.Models
{
    public class Product : CAD
    {
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public Product():base() {}
        public Product(string descricao, decimal valor):base() {
            Descricao = descricao;
            Valor = valor;
        }
    }
}