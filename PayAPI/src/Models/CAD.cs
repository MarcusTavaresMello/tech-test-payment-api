using System;

namespace PayAPI.Models
{
    public abstract class CAD
    {
        public Guid Id { get; protected set; }
        public CAD()
        {
            Id = Guid.NewGuid();
        }
    }
}