using System;

namespace PayAPI.Models
{
    public class ItemCAD : CAD
    {
        public Guid IdVenda { get; set; }
        public Product Produto { get; set; }
        public decimal Quantidade { get; set; }
        public decimal getValor () => Math.Round(this.Quantidade * this.Produto.Valor, 2);
        public ItemCAD():base() {}
        public ItemCAD(Product produto, decimal quantidade):base()
        {
            this.Produto = produto;
            this.Quantidade = quantidade;
        }

    }
}