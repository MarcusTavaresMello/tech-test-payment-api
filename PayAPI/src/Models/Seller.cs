using System;

namespace PayAPI.Models
{
    public class Seller : CAD
    {
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int Telefone { get; set; }

        public Seller():base() {}
        public Seller(string cpf, string nome, string email, int telefone):base()
        {
            this.CPF = cpf;
            this.Nome = nome;
            this.Email = email;
            this.Telefone = telefone;
        }

    }
}