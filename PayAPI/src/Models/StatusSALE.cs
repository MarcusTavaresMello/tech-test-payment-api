using System;

namespace PayAPI.Models
{
    public class StatusSALE
    {
        public const string aguardando_pgto = "Aguardando pagamento";
        public const string pgto_aprovado = "Pagamento aprovado";
        public const string enviado = "Enviado para transportadora";
        public const string prod_entregue = "Entregue";
        public const string venda_cancelada = "Cancelada";
        public string Status { get; set; }
        public StatusSALE() {}
        public StatusSALE(string status)
        {
            Status = status;
        }
    }

}