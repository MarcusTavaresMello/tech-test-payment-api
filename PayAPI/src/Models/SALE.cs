using System;
using System.Collections.Generic;
using System.Linq;

namespace PayAPI.Models
{
    public class SALE : CAD
    {
        public DateTime Data { get; private set; }
        public Seller Vendedor { get; set; }
        public IEnumerable<ItemCAD> Itens { get; set; }
        private string _status = "";
        public string Status
        {
            get => _status;
            set => this.modifyStatus(value);
        }
        public decimal ValorTotal => Math.Round(this.Itens?.Sum(itens => itens.getValor()) ?? 0, 2);
        public SALE():base() {}
        public SALE(Seller vendedor, IEnumerable<ItemCAD> itens) : base()
        {
            Data = DateTime.Now;
            Vendedor = vendedor;
            Itens = itens;
            Status = StatusSALE.aguardando_pgto;

            foreach(var item in itens)
            {
                item.IdVenda = this.Id;
            }
        }


        private string _txtStatusNoModify(string status)
        {
            return $"Não é possível a alteração do status : {status}.";
        }
        private string _validation(string newStatusSale, string[] statusValid)
        {
            if (statusValid.Contains(newStatusSale))
            {
                this._status = newStatusSale;
                return "";
            }
            else
                return $"O status atual da venda '{this._status}' somente poderá ser alterado para '{string.Join("' ou '", statusValid)}'";
        }


        public string modifyStatus(string statusSale)
        {
            switch (this._status)
            {
                case "":
                  this._status = statusSale;
                  return "";

                case StatusSALE.aguardando_pgto:
                    return _validation(statusSale, new string[] {
                        StatusSALE.pgto_aprovado, StatusSALE.venda_cancelada });

                case StatusSALE.pgto_aprovado:
                    return _validation(statusSale, new string[] {
                         StatusSALE.enviado, StatusSALE.venda_cancelada });

                case StatusSALE.enviado:
                    return _validation(statusSale, new string[] { StatusSALE.prod_entregue });

                case StatusSALE.venda_cancelada:
                case StatusSALE.prod_entregue:
                    return _txtStatusNoModify(this._status);

                default:
                    return "";
            }

        }
    }
}