using System;
using System.Collections.Generic;
using PayAPI.Models;

namespace PayAPI.Views
{
    public class SaleView
    {
        public Guid Id { get; set; }
        public DateTime Data { get; set; }
        public Seller Vendedor { get; set; }
        public IEnumerable<ItemCAD> Itens { get; set; }
        public string Status { get; set; }
        public SaleView() {}
        public SaleView(Guid id, DateTime data, string status, Seller vendedor, IEnumerable<ItemCAD> itens)
        
        {
            Id = id;
            Data = data;
            Vendedor = vendedor;
            Itens = itens;
            Status = StatusSALE.aguardando_pgto;
        }

    }

}