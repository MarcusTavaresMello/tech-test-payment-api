using System;
using System.Threading.Tasks;
using PayAPI.Models;
using System.Collections.Generic;
using PayAPI.Views;


namespace PayAPI.Repositories
{
    public class SaleRepository
    {
        private List<SALE> Vendas = new List<SALE>();
        public Task<SALE> newSALE(SALE venda)
        {
            SALE newSale = new SALE(venda.Vendedor, venda.Itens);
            this.Vendas.Add(newSale);
            return Task.FromResult(newSale);
        }


        public Task<SALE> getSALE(Guid idVenda)
        {
            SALE vendaRegistrada = this.Vendas.Find(v => v.Id == idVenda);
            return Task.FromResult(vendaRegistrada);
        }


        public void modifyStatus(SALE venda)
        {
            SALE vendaRegistrada = this.Vendas.Find(v => v.Id == venda.Id);
            vendaRegistrada = venda;
        }
    }
}